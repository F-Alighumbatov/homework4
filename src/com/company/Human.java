package com.company;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Human mother;
    private Human father;
    private Pet pet;
    public String[][] schedule = new String[7][2];


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Human() {

    }


    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;

    }

    public Human(String name, String surname, int year, Human mother, Human father) {

        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;

    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.schedule = schedule;


    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append(name != null && !name.isEmpty() ? "name='" + name + ", " : "");
        str.append(surname != null && !surname.isEmpty() ? "surname='" + surname + ", "  : "");
        str.append(year != 0 ? "year='" + year + ", "  : "");
        str.append(iq != 0 ? "iq='" + iq + ", "  : "");
        str.append(mother != null ? "mother= " + mother.name + ", "  : "");
        str.append( father != null ? "father= " + father.name + ", "  : "");
        str.append( pet != null ? "pet='" + pet + ", " : "");
        str.append(schedule!=null ? "scheduler= " + schedule + ", " : "");

        // en sonuncu field'den sonra ,  olmamalidir, ona gore silirem sondan iki simvolu
        str.delete(str.length() - 2, str.length() + 1);

        return str.toString();
    }



}

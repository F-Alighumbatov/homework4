package com.company;

import java.util.Arrays;

public class Pet {

    private String species;
    private String nickname = null;
    private int age;
    private int trickLevel;
    private String[] habits;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }




    public String eat() {
        String s = "I am eating";

        return s;
    }

    public String respond() {
        String s = "Hello, owner. I am ";
        //String a = nickname;
        String b = ". I miss you!";
        s.concat(getNickname()).concat(b);

        return s.concat(getNickname()).concat(b);
    }


}
